import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import DriverManager from "../services/DriverManager";


export default class Server {
	private port: number;
	private app: express.Application;

	public constructor(app: express.Application, port: number) {
		this.port = port;
		this.app = app;

		this.configApp();
		this.setRoutes();

		DriverManager.Instance.connect();

		this.startServer();
	}

	private startServer() {
		this.app.listen(this.port, () => {
			console.log(`Server started at http://localhost:${this.port}!`);
		});
	}

	private configApp() {
		// parse application/x-www-form-urlencoded from body
		this.app.use(bodyParser.json());

		//parse application.json from body
		this.app.use(bodyParser.urlencoded({ extended: false }));

		this.app.use(cors());
	}

	private setRoutes() {
		this.app.get(
			"/",
			(request: express.Request, response: express.Response) => {
				response.send("Hello world");
			}
		);

		this.app.get(
			"/lists",
			async (req: express.Request, res: express.Response) => {
				try {
					let lists = await DriverManager.Instance.getAllToDoLists();
					if (lists.length > 0) {
						res.send(lists);
					} else {
						res.send("No list found!");
					}
				} catch {
					res.send("Error!");
				}
			}
		);

		this.app.get(
			"/list/:name/:title",
			async (req: express.Request, res: express.Response) => {
				let nameFromRequest = req.params.name;
				let titleFromRequest = req.params.title;
				try {
					let requestList = await DriverManager.Instance.getListByName(
						nameFromRequest
					);
					if(requestList)
					{
						res.send(requestList.listElements.find(el => el.title = titleFromRequest));
					}
					res.send
				} catch {
					res.send("Error!");
				}
			}
		);

		this.app.post(
			"/addList",
			async (req: express.Request, res: express.Response) => {
				try {
					await DriverManager.Instance.addNewList(req.body.name);
					res.send("Success!");
				} catch {
					res.send("Error!");
				}
			}
		);

		this.app.delete(
			"/deleteList/:name",
			async(req: express.Request, res: express.Response) =>{
				let nameFromRequest = req.params.name;
				try {
					await DriverManager.Instance.deleteList(nameFromRequest);
					res.send("List Deleted");
				} catch {
					res.send("Error!");
				}
			}

		);
		this.app.post(
			"/addListElement",
			async (req: express.Request, res: express.Response) => {
				try {
					await DriverManager.Instance.addNewElementForList(
						req.body.listName,
						req.body.taskName,
						req.body.description
					);

					res.send("Success!");
				} catch {
					res.send("Error!");
				}
			}
		);

		this.app.delete(
			"/deleteListElement/:listTitle/:listElement",
			async(req: express.Request, res: express.Response) =>{
				let listTitleFromRequest = req.params.listTitle;
				let listElementFromRequest = req.params.listElement;
				try {
					await DriverManager.Instance.deleteListElement(
						listTitleFromRequest,
						listElementFromRequest
					);
					res.send("LIst item deleted");
				} catch{
					res.send("Error!");
				}
			}

		);
	}
}
